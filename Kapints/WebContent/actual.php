<?php
while ( $datas [$index] ['date'] < new DateTime ( 'now -1 day' ) && $index < count ( $datas ) ) {
	$index = $index + 1;
}
?>

<!-- Jumbotron Header -->
<div class="jumbotron row">
	<h2>Next Event</h2>
	<div class="col-md-4 col-sm-5">
		<div class="thumbnail">
			<img src="<?php echo getImage($datas[$index]['name']);?>" />
			<div class="event-number">
				<p style="line-height: 40px; text-align: center; font-size: 14px !important;">
					<b><?php echo $datas [$index] ['idx'] ?></b>
				</p>
			</div>
		</div>
	</div>
	<div class="col-md-8 col-sm-7">
		<h2><?php
		if (isset ( $datas [$index] ['url'] )) {
			echo '<a target="blank" href="' . $datas [$index] ['url'] . '">';
		}
		
		echo $datas [$index] ['name'];
		
		if (isset ( $datas [$index] ['url'] )) {
			echo '</a>';
		}
		?>
				</h2>
		<p>am <?php echo getFormattedDate($datas[$index]['date']);?></p>
		<p>
			<a target="blank"
				href="https://nominatim.openstreetmap.org/search.php?q=<?php echo urlencode($datas[$index]['name'].', Karlsruhe');?>"
				class="btn btn-primary">OSM</a> <a target="blank"
				href="https://www.google.com/maps?q=<?php echo urlencode($datas[$index]['name'].', Karlsruhe');?>"
				class="btn btn-default">Google Maps</a>
		</p>
	</div>
</div>

<hr>

<!-- Title -->
<div class="row">
	<div class="col-lg-12">
		<h3>Following Events</h3>
	</div>
</div>
<!-- /.row -->

<!-- Page Features -->
<div class="row text-center">

		<?php
		$index ++;
		for(; $index < count ( $datas ); $index ++) {
			?>
			<div class="col-md-3 col-sm-6 hero-feature">
		<div class="thumbnail">
			<img src="<?php print getImage($datas[$index]['name']);?>" class="feaure-img" alt="">
			<div class="event-number">
				<p style="line-height: 40px; text-align: center;">
					<b><?php echo $datas [$index] ['idx'] ?></b>
				</p>
			</div>
			<div class="caption">
				<h3><?php
			if (isset ( $datas [$index] ['url'] )) {
				echo '<a target="blank" href="' . $datas [$index] ['url'] . '">';
			}
			
			echo $datas [$index] ['name'];
			
			if (isset ( $datas [$index] ['url'] )) {
				echo '</a>';
			}
			?></h3>
				<p>am <?php echo getFormattedDate($datas[$index]['date']);?></p>
				<p>
					<a target="blank"
						href="https://nominatim.openstreetmap.org/search.php?q=<?php echo urlencode($datas[$index]['name'].', Karlsruhe');?>"
						class="btn btn-primary">OSM</a> <a target="blank"
						href="https://www.google.com/maps?q=<?php echo urlencode($datas[$index]['name'].', Karlsruhe');?>"
						class="btn btn-default">Google Maps</a>
				</p>
			</div>
		</div>
	</div>
			
			<?php
		}
		?>


		</div>
<!-- /.row -->

